<?php
// problemas con servidor
// http://askubuntu.com/questions/450825/ubuntu-14-04-phpmyadmin-cant-find-mcrypt-extension


/**
 *
 * CakePHP Component to interact with the Sermepa TPV service
 *
 * Copyright 2014 Bernat Arlandis i Mañó
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * @copyright Copyright 2014 Bernat Arlandis i Mañó
 * @link http://bernatarlandis.com
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPLv2
 */

App::uses('AppController', 'Controller');
App::import('Vendor', 'Redsys.Redsys', array('file' => 'Redsys'.DS.'redsys.php'));


class RedsysComponent extends Component {

	function new_form($order = array()){

		try{
			
		

			
			$order['amount'] = strval($order['amount']);
			$order['amount'] = str_replace(".", "", $order['amount']);
			$order['amount'] = str_replace(",", "", $order['amount']);
			$pasarela = new Sermepa();
			$pasarela->setParameter("DS_MERCHANT_AMOUNT",$order['amount']);
			$pasarela->setParameter("DS_MERCHANT_ORDER",$order['order_number']);
			$pasarela->setParameter("DS_MERCHANT_MERCHANTCODE",$order['fuc']);
			$pasarela->setParameter("DS_MERCHANT_CURRENCY",'978');
			$pasarela->setParameter("DS_MERCHANT_TRANSACTIONTYPE","0");
			$pasarela->setParameter("DS_MERCHANT_TERMINAL",'1');
			$pasarela->setParameter("DS_MERCHANT_MERCHANTURL",$order['url_notification']);
			$pasarela->setParameter("DS_MERCHANT_URLOK",$order['url_ok']);		
			$pasarela->setParameter("DS_MERCHANT_URLKO",$order['url_ko']);

			//Datos de configuración
			$version="HMAC_SHA256_V1";
			$kc = $order['key'];//Clave recuperada de CANALES
			//$kc = 'Mk9m98IfEblmPfrpsawt7BmxObt98Jev';
			// Se generan los parámetros de la petición
			$request = "";
			$params = $pasarela->createMerchantParameters();
			$signature = $pasarela->createMerchantSignature($kc);
			//echo $signature;

	        //Generamos botón submit
	        $pasarela->submit('nombre_submit','Pagar');
	    }
	    catch(Exception $e){
	        echo $e->getMessage();   
	    }
		    $pasarela->ejecutarRedireccion($version); 
		    //echo $pasarela->create_form($version);
		}	

}

