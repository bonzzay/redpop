<?php
App::uses('AppModel', 'Model');
App::import('Model','Content');
/**
 * Menu Model
 *
 */
class Menu extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'sort_order' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'position' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	function staticContent(){
		$options = array(
		    /*__('calendar/cena-mas-espectaculo')=>__('Cena + Espectáculo'),
		    __('calendar/solocena')=>__('Espectáculo'),
		    __('galeria-de-fotos')=>__('Gallería fotos Flickr'),
		    __('fotos-y-videos-portada')=>__('Gallería fotos y videos'),
			__('artists')=>__('Artistas'),
			__('localizacion-contacto')=>__('Localización'),*/


		   
		);
		return $options;
	}

	function afterFind($results, $primary = false){
		
		$reut = array();
		$i = 0;
		foreach($results as $result){

			$model = $result['Menu']['model'];
			$foreign_key = $result['Menu']['foreign_key'];
			$url = $result['Menu']['url'];

			if($model != null){
			
				$this->Content = Classregistry::init('Content');

		        
		        $content = $this->Content->ver($foreign_key);
		  		$result['Page']['slug'] = $content['Content']['slug'];
		        $reut[$i] = $result;
			}else{
				$result['Page']['slug'] = $url;
		        $reut[$i] = $result;
			}
			
	        $i++;



		}
		
		return $reut;
	}
}
