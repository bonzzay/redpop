<?php
App::uses('AppController', 'Controller');
/**
 * Menus Controller
 *
 * @property Menu $Menu
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class MenusController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $theme = "Cakestrap";
	public $components = array('Paginator', 'Flash', 'Session', 'Auth');
	public $uses = array('Menu', 'Content');

	public function beforeFilter() {
		$this->Auth->Allow('login');
		$this->Auth->logoutRedirect = array('/admin/users/login');
		
		$this->Auth->authenticate = array(
		    AuthComponent::ALL => array('userModel' => 'User'),
		    'Form'=> array(
                'fields' => array('username' => 'email'),
		    'Basic'));
		$this->Auth->authError = "Please log in first in order to preform that action.";

	
		
	}

	public function beforeRender() {
		
	
		
	}



	public function reorder() {
		foreach ($this->data['Menu'] as $key => $value) {
			$this->Menu->id = $value;
			$this->Menu->saveField("sort_order",$key + 1);
		}
		//$this->log(print_r($this->data,true));
		exit();
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index($id = null) {
		$this->Menu->recursive = 0;
		$this->set('menus', $this->Menu->find('all',array(
			'order' => 'sort_order ASC','conditions'=> array('Menu.position'=> $id)
			)
		));
		if($id == 1){
			$this->set('menuname', 'Menu Top');
			$this->set('position', $id);
		}else if($id == 2){
			$this->set('menuname', 'Menu Footer Producto');
			$this->set('position', $id);
		}else if($id == 3){
			$this->set('menuname', 'Menu Footer Nosotros');
			$this->set('position', $id);
		}else if($id == 4){
			$this->set('menuname', 'Menu Footer Ayuda');
			$this->set('position', $id);
		}
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Menu->exists($id)) {
			throw new NotFoundException(__('Invalid menu'));
		}
		$options = array('conditions' => array('Menu.' . $this->Menu->primaryKey => $id));
		$this->set('menu', $this->Menu->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add($pos = null) {
		if ($this->request->is('post')) {
			$this->Menu->create();
			$data = $this->creaturl($this->request->data['Menu']['link']);
			$this->request->data['Menu']['model'] = $data['Menu']['model'];
			$this->request->data['Menu']['foreign_key'] = $data['Menu']['foreign_key'];
			$this->request->data['Menu']['url'] = $data['Menu']['url'];

			if ($this->Menu->save($this->request->data)) {
				$this->Session->setFlash(__('The menu has been saved'), 'flash/success');
				$this->redirect('/admin/menus/'.$this->request->data['Menu']['position']);
			} else {
				$this->Session->setFlash(__('The menu could not be saved. Please, try again.'), 'flash/error');
			}
		}
		$this->set('pos', $pos);
		$this->Content->locale = $this->Session->read('Lang.idioma');
		$this->set('contents', $this->Content->find('all'));
		$this->set('statics', $this->Menu->staticContent());
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
        $this->Menu->id = $id;
		if (!$this->Menu->exists($id)) {
			throw new NotFoundException(__('Invalid menu'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			$data = $this->creaturl($this->request->data['Menu']['link']);
			$this->request->data['Menu']['model'] = $data['Menu']['model'];
			$this->request->data['Menu']['foreign_key'] = $data['Menu']['foreign_key'];
			$this->request->data['Menu']['url'] = $data['Menu']['url'];
			if ($this->Menu->save($this->request->data)) {
				$this->Session->setFlash(__('The menu has been saved'), 'flash/success');
				$this->redirect('/admin/menus/'.$this->request->data['Menu']['position']);
			} else {
				$this->Session->setFlash(__('The menu could not be saved. Please, try again.'), 'flash/error');
				$data = $this->request->data = $this->Menu->find('first', $options);
				$this->set('menu', $data);
			}
		} else {
			$options = array('conditions' => array('Menu.' . $this->Menu->primaryKey => $id));
			$this->request->data = $this->Menu->find('first', $options);
		}
		$this->set('contents', $this->Content->find('all'));
		$this->set('statics', $this->Menu->staticContent());
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Menu->id = $id;
		if (!$this->Menu->exists()) {
			throw new NotFoundException(__('Invalid menu'));
		}
		if ($this->Menu->delete()) {
			$this->Session->setFlash(__('Menu deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Menu was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}

	private function creaturl($link){
		$porciones = explode("#", $link);
	
		
		if(isset($porciones[2])){
			
			$data['Menu']['model'] = $porciones[1];
			$data['Menu']['foreign_key'] = $porciones[2];
			$data['Menu']['url'] = '';
		}else{

			$data['Menu']['model'] = '';
			$data['Menu']['foreign_key'] = '';
			$data['Menu']['url'] = $porciones[0];
		}
		
		return $data;

	}

}
