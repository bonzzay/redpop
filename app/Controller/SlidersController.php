<?php
App::uses('AppController', 'Controller');
/**
 * Sliders Controller
 *
 * @property Slider $Slider
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SlidersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $theme = "Cakestrap";
	public $components = array('Paginator', 'Flash', 'Session','Auth');
	//public $helpers = array('Wysiwyg.Wysiwyg' => array('editor' => 'Ck'));

	public function beforeFilter() {
		$this->Auth->Allow('login');
		$this->Auth->logoutRedirect = array('/admin/users/login');
		
		$this->Auth->authenticate = array(
		    AuthComponent::ALL => array('userModel' => 'User'),
		    'Form'=> array(
                'fields' => array('username' => 'email'),
		    'Basic'));
		$this->Auth->authError = "Please log in first in order to preform that action.";

	
		
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Slider->recursive = 0;
		$this->set('sliders', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Slider->exists($id)) {
			throw new NotFoundException(__('Invalid slider'));
		}
		$options = array('conditions' => array('Slider.' . $this->Slider->primaryKey => $id));
		$this->set('slider', $this->Slider->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Slider->create();
			if ($this->Slider->save($this->request->data)) {
				$this->Session->setFlash(__('The slider has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The slider could not be saved. Please, try again.'), 'flash/error');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
        $this->Slider->id = $id;
		if (!$this->Slider->exists($id)) {
			throw new NotFoundException(__('Invalid slider'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			
			if(!isset($this->request->data['Slider']['active'])){
				$this->request->data['Slider']['active']= 0;
			}
			if ($this->Slider->save($this->request->data)) {
				$this->Session->setFlash(__('The slider has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The slider could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Slider.' . $this->Slider->primaryKey => $id));
			$this->request->data = $this->Slider->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Slider->id = $id;
		if (!$this->Slider->exists()) {
			throw new NotFoundException(__('Invalid slider'));
		}
		if ($this->Slider->delete()) {
			$this->Session->setFlash(__('Slider deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Slider was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}
}
