<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array('Category', 'Menu', 'Product', 'Content', 'Faqcategory', 'Faq', 'Post', 'Slider');
	public $components = array('Paginator', 'Session','Email', 'Cookie', 'Mailchimp');

	public function beforeFilter() {
		//setLocale(LC_ALL, "es_ES");
		//$this->_setLanguage();

		//seo
		$this->set('title_for_layout', 'Redpop');
		$this->set('description_for_layout', 'Redpop');
		$this->set('keywords_for_layout', 'Redpop');

		//donde estoy en el menú
		$this->set('menuactive', 'shop'); //default

		
	}
	public function beforeRender(){
		
		
		//Menus
		
		/*$footerproduct = $this->Menu->find('all', array('conditions'=> array('Menu.position' => 2), 'order' => array('Menu.sort_order ASC')));*/

		$footerproduct = $this->Category->find('all',array('limit'=>5, 'order' => array('Category.id ASC')));
		$this->set('footerproduct',$footerproduct);
		$footerabout = $this->Menu->find('all', array('conditions'=> array('Menu.position' => 3), 'order' => array('Menu.sort_order ASC')));
		$this->set('footerabout',$footerabout) ;
		$footerhelp = $this->Menu->find('all', array('conditions'=> array('Menu.position' => 4), 'order' => array('Menu.sort_order ASC')));
		$this->set('footerhelp',$footerhelp) ;

		// categories
		$this->Category->recursive = 2;
		//$this->Category->unbindModel(array('hasMany'=> array('Product')));
		$categories = $this->Category->find('all', array('conditions' => array('Category.parent_id'=> 0)));
		$this->set('categories',$categories) ;
		
	}

/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */
	public function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			return $this->redirect('/');
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact('page', 'subpage', 'title_for_layout'));

		try {
			$this->render(implode('/', $path));
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
	}

	public function home(){
		$this->layout = 'home';
		// slider
		;
		$this->set('sliders', $this->Slider->find('all', array('conditions'=> array('Slider.active'=> 1))));


	}
	public function about(){
		$this->set('menuactive', 'about'); //default
	}
	public function contact(){
		$this->set('menuactive', 'contact'); //default
		if($this->request->is('post')) {
			if($this->request->data['Contact']['name'] == '' or $this->request->data['Contact']['email'] == '' or $this->request->data['Contact']['message'] == ''){
				
				 $this->set('messagealert', __('Error sending message')); 
			}else{
				$this->_sendContactEmail($this->request->data);
				$this->set('messagealert', __('Message sent correctly'));

			}
			
		}
	}
	public function solutions(){
		$this->set('menuactive', 'solutions'); //default

	}
	public function clients(){
		$this->set('menuactive', 'clients'); //default
	}


	public function category($slug = null){

		// is category
		$this->Category->recursive = 2;
		$this->Category->Product->unbindModel( array('belongsTo' => array('Category')));
		$this->Category->Product->unbindModel( array('hasAndBelongsToMany' => array('Attribute')));
		$category = $this->Category->find('first', array('conditions' =>array('Category.slug'=>$slug )));
		;

		if(empty($category)){
				$this->content($slug);
		}else{
			$this->set('category', $category);
			//seo
			$this->pageTitle = $category['Category']['seo_title'];
			$this->pageDescription = $category['Category']['seo_description'];
			$this->pageKeys = $category['Category']['seo_keys'];
		}

		// is content

	}

	public function product($slug = null, $slugp = null){

		$product = $this->Product->find('first', array('conditions' =>array('Product.slug'=>$slugp )));
		
		if(!$product){
			
			throw new NotFoundException();
			
		}else{
			
			$this->set('product', $product);
			//seo
			$this->pageTitle = $product['Product']['seo_title'];
			$this->pageDescription = $product['Product']['seo_description'];
			$this->pageKeys = $product['Product']['seo_keys'];
		}

	}

	public function content($slug =null){

		
		$content = $this->Content->find('first', array('conditions' =>array('Content.slug'=>$slug )));
		
		
		if (!$content) {
			throw new NotFoundException();
			
		}

		$this->set('content', $content);
		//seo

		
		$this->pageTitle = $content['Content']['seo_title'];
		$this->pageDescription = $content['Content']['seo_description'];
		$this->pageKeys = $content['Content']['seo_keys'];
		$this->render('content');
	}

	public function faq($id = null){

		//Faqcategory
		if($id != null){
			$faqi = $this->Faq->find('first', array( 'conditions' => array('Faq.id'=>$id) ));
			
			$this->set('faqi', $faqi);
			$this->set('menuactive', 'support'); //default
			$this->render('fq');
		}else{
			$faqis = $this->Faqcategory->find('all', array( 'order' => array('Faqcategory.order ASC') ));
			$this->set('faqis', $faqis);
			$this->set('menuactive', 'support'); //default
		}


		
		
		
	}

	public function posts($slug = null){

		$this->set('menuactive', 'blog'); //default
		if($slug == null){
			// all posts
			$this->pageTitle = __('Redpop');
			$this->pageDescription = __('ERedpop');
			$this->pageKeys = __('Redpop');
			
			//$posts = $this->Post->find('all', array('limit' =>'40' ));
			$this->paginate = array('limit' =>'40', 'order' => 'id DESC' );
			$posts = $this->paginate('Post');
			$this->set('posts',$posts);

		}else{
			//one post
			$post = $this->Post->find('first', array('conditions' =>array('Post.slug'=> $slug) ));
			$this->set('post',$post);

			//seo
			$this->pageTitle = $post['Post']['seo_title'];
			$this->pageDescription = $post['Post']['seo_description'];
			$this->pageKeys = $post['Post']['seo_keys'];
			$this->render('post');
		}
	}

	public function copyright(){
		$this->set('menuactive', 'home'); //default
	}

	public function newsletter(){
		$this->Mailchimp->api('10ae6335948a92996e01f60ccb4c4c7f-us10');
        $lists = $this->Mailchimp->call('lists/list');
    
        if ($this->request->is('post')) {
			
			if ($this->request->data['Mailchimp']['email'] != null) {
				// Save in Item
				$sub = $this->listsubscribe('0284ca1e67', $this->request->data['Mailchimp']['email']);

				$this->Session->setFlash(__('Subscrito a nuestra newsletter'), 'message');
				$this->redirect($this->referer());
			} else {
				$this->Session->setFlash(__('Campo de email no debe estar vacío, y debe acceptar los términos de privacidad.'), 'error');
				$this->redirect($this->referer());
			}
		}
	}

	private function listsubscribe($lidlist = null, $email = null){
        $this->Mailchimp->api('10ae6335948a92996e01f60ccb4c4c7f-us10');
        $lists = $this->Mailchimp->call('lists/subscribe', array(
            'id'                => $lidlist,
            'email'             => array('email'=>$email),
            //'merge_vars'        => array('FNAME'=>$name, 'LNAME'=>$lname),
            'double_optin'      => false,
            'update_existing'   => true,
            'replace_interests' => false,
            'send_welcome'      => false,
        ));
        //return $lists;
        return true;

        
    }

	private function _sendContactEmail($user){
		$this->Email->to = 'info@qracks.com';
		$this->Email->subject = __('formulario redpop web');
		$this->Email->from = 'Redpop <no-reply@redpop.com>';
		$this->Email->template = 'contact';
		$this->Email->sendAs = 'both';
		$this->set('user', $user);
		$this->Email->send();
  }

	
}
