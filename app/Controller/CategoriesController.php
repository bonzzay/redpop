<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Category $Category
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class CategoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $theme = "Cakestrap";
	public $components = array('Paginator', 'Flash', 'Session','Auth');

	public function beforeFilter() {
		$this->Auth->Allow('login');
		$this->Auth->logoutRedirect = array('/admin/users/login');
		
		$this->Auth->authenticate = array(
		    AuthComponent::ALL => array('userModel' => 'User'),
		    'Form'=> array(
                'fields' => array('username' => 'email'),
		    'Basic'));
		$this->Auth->authError = "Please log in first in order to preform that action.";

	
		
	}



/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->data = $this->Category->generateTreeList(null, null, null, '&nbsp;&nbsp;&nbsp;');

        $this->set('categorylist',$this->data);
	}


/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if (!empty($this->data)) {
            $this->Category->save($this->data);
            $this->redirect(array('action'=>'index'));
        } else {
            $parents[0] = "[ No Parent ]";
            $nodelist = $this->Category->generateTreeList(null,null,null," - ");
            if($nodelist)
                foreach ($nodelist as $key=>$value)
                    $parents[$key] = $value;
            $this->set(compact('parents'));
        }
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
        if (!empty($this->data)) {
            if($this->Category->save($this->data)==false)
                $this->Session->setFlash('Error saving Categoria.');
            $this->redirect(array('action'=>'index'));
        } else {
            if($id==null) die("No ID received");
            $this->data = $this->Category->read(null, $id);
            $parents[0] = "[ No Parent ]";
            $nodelist = $this->Category->generateTreeList(null,null,null," - ");
            if($nodelist) 
                foreach ($nodelist as $key=>$value)
                    $parents[$key] = $value;
            $this->set(compact('parents'));
        }
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		
		$this->Category->id = $id;
		if (!$this->Category->exists()) {
			throw new NotFoundException(__('Invalid category'));
		}
		if ($this->Category->delete()) {
			$this->Session->setFlash(__('Category deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Category was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}

	public function admin_moveup($id=null) {
        if($id==null)
            die("No ID received");
        $this->Category->id=$id;
        if($this->Category->moveUp()==false)
            $this->Session->setFlash('The Category could not be moved up.');
        $this->redirect(array('action'=>'index'));
    }

    public function admin_movedown($id=null) {
        if($id==null)
            die("No ID received");
        $this->Category->id=$id;
        if($this->Category->moveDown()==false)
            $this->Session->setFlash('The Category could not be moved down.');
        $this->redirect(array('action'=>'index'));
    }
}
