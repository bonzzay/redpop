<?php
App::uses('AppController', 'Controller');
/**
 * Faqcategories Controller
 *
 * @property Faqcategory $Faqcategory
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class FaqcategoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $theme = "Cakestrap";
	public $components = array('Paginator', 'Session', 'Auth');

	public function beforeFilter() {
		$this->Auth->Allow('login');
		$this->Auth->logoutRedirect = array('/admin/users/login');
		
		$this->Auth->authenticate = array(
		    AuthComponent::ALL => array('userModel' => 'User'),
		    'Form'=> array(
                'fields' => array('username' => 'email'),
		    'Basic'));
		$this->Auth->authError = "Please log in first in order to preform that action.";

	
		
	}
	


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Faqcategory->recursive = 0;
		$this->set('faqcategories', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Faqcategory->exists($id)) {
			throw new NotFoundException(__('Invalid faqcategory'));
		}
		$options = array('conditions' => array('Faqcategory.' . $this->Faqcategory->primaryKey => $id));
		$this->set('faqcategory', $this->Faqcategory->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Faqcategory->create();
			if ($this->Faqcategory->save($this->request->data)) {
				$this->Session->setFlash(__('The faqcategory has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The faqcategory could not be saved. Please, try again.'), 'flash/error');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
        $this->Faqcategory->id = $id;
		if (!$this->Faqcategory->exists($id)) {
			throw new NotFoundException(__('Invalid faqcategory'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Faqcategory->save($this->request->data)) {
				$this->Session->setFlash(__('The faqcategory has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The faqcategory could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Faqcategory.' . $this->Faqcategory->primaryKey => $id));
			$this->request->data = $this->Faqcategory->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Faqcategory->id = $id;
		if (!$this->Faqcategory->exists()) {
			throw new NotFoundException(__('Invalid faqcategory'));
		}
		if ($this->Faqcategory->delete()) {
			$this->Session->setFlash(__('Faqcategory deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Faqcategory was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}
}
