<!-- container starts -->
<section class="plv_main">
	<div class="container">
		<h2><? echo __('SOLUTIONS');?></h2>
        <div class="textdescription">
            <p><? echo __("Q-Rack is the first Patented multi awarded product design to meet the display packaging, labor saving and environmental directives from WAL-MART, the dominant retailer in the United States. It is a disruptive technology within the materials handling and packaging industry, as it is the only current market solution that designed to go from product loading at the end of a manufacturers’ production line onto a delivery truck and then straight to the retailer sales floor TRANSFORMING ITSELF INTO POINT OF PURCHASE MATERIAL, eliminating layers of costs while simultaneously increasing sales per square foot.");?></p>
        </div>
        <div>
        <ul>
            <? $actual= 0;?>
            <? foreach ($categories as $cat):?>
               
                <li <? if($actual%2!=0){ echo 'class="last"';};?>>
                    
                    <? if($cat['Category']['image'] != null):?>


                        <img src="<? echo $this->Html->url('/');?>files/category/image/<? echo $cat['Category']['image_dir'];?>/thumb_<? echo $cat['Category']['image'];?>" width="550" height="340" alt="img" >
                    <? else:?>
                        
                         <? echo  $this->Html->image('/theme/Bonzzay/images/plv_img1.jpg', array('alt'=>'img', 'width' => '550' , 'height' => '340'));?>
                    <? endif;?>
                    <a href="<? echo $this->Html->url('/');?><? echo $cat['Category']['slug'];?>"><span><? echo $cat['Category']['name'];?></span></a>
                </li>
                 <? $actual++;?>
            <? endforeach;?>
        	
        </ul>
        </div>
        <div class="clear"></div>
    </div>
</section>
<!-- container ends -->