<!-- container starts -->
<section class="plv_main">
	<div class="container">
		<h2><? echo $content['Content']['title'];?></h2>
        <? if($content['Content']['image'] != null):?>
            <div class="textdescription">
                <img src="<? echo $this->Html->url('/');?>files/content/image/<? echo $content['Content']['image_dir'];?>/thumb_<? echo $content['Content']['image'];?>" width="1200" height="400" alt="img" >
            </div>
        <? endif;?>
        <div>
            <? echo $content['Content']['text'];?>
        </div>
        <div class="clear"></div>
    </div>
</section>
<!-- container ends -->