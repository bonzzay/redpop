<!-- container starts -->
<section class="plv_main">
	<div class="container">
		<h2><? echo __("CLIENTS");?></h2>
       
        <div class="textdescription">
	        <ul class="clients">
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/3m.png', array('alt'=>'3m', 'width' => '112' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/abbott.png', array('alt'=>'abbott', 'width' => '242' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/bimbo.jpg', array('alt'=>'bimbo', 'width' => '70' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/buchanans.jpg', array('alt'=>'buchanans', 'width' => '316' , 'height' => '60'));?>
	        	</li>
	        	
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/calvo.png', array('alt'=>'calvo', 'width' => '140' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/campbells.jpg', array('alt'=>'campbells', 'width' => '205' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/clorox.jpg', array('alt'=>'clorox', 'width' => '99' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/cocacola.png', array('alt'=>'cocacola', 'width' => '183' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/corona.jpg', array('alt'=>'corona', 'width' => '87' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/crayola.jpg', array('alt'=>'crayola', 'width' => '96' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/cuetara.png', array('alt'=>'cuetara', 'width' => '123' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/danone.png', array('alt'=>'danone', 'width' => '123' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/diageo.jpg', array('alt'=>'diageo', 'width' => '280' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/disney.jpg', array('alt'=>'disney', 'width' => '111' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/fanta.png', array('alt'=>'fanta', 'width' => '72' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/ferrero.jpg', array('alt'=>'ferrero', 'width' => '128' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/finland.png', array('alt'=>'finland', 'width' => '85' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/generalmills.jpg', array('alt'=>'generalmills', 'width' => '103' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/greenies.png', array('alt'=>'greenies', 'width' => '111' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/hasbro.jpg', array('alt'=>'hasbro', 'width' => '111' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/heineken.jpg', array('alt'=>'heineken', 'width' => '81' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/hersheys.jpg', array('alt'=>'hersheys', 'width' => '180' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/johnson.jpg', array('alt'=>'johnson', 'width' => '247' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/jumex.jpg', array('alt'=>'jumex', 'width' => '158' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/karcher.jpg', array('alt'=>'karcher', 'width' => '211' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/kelloggs.jpg', array('alt'=>'kelloggs', 'width' => '156' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/kraftfoods.jpg', array('alt'=>'kraftfoods', 'width' => '180' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/mars.jpg', array('alt'=>'mars', 'width' => '196' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/miracle.jpg', array('alt'=>'miracle', 'width' => '64' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/nestle.jpg', array('alt'=>'nestle', 'width' => '60' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/oroweat.jpg', array('alt'=>'oroweat', 'width' => '93' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/ortho.jpg', array('alt'=>'ortho', 'width' => '112' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/pepsi.jpg', array('alt'=>'pepsi', 'width' => '44' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/pg.jpg', array('alt'=>'pg', 'width' => '138' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/quaker.jpg', array('alt'=>'quaker', 'width' => '70' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/reckitt.jpg', array('alt'=>'reckitt', 'width' => '137' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/sabmiller.jpg', array('alt'=>'sabmiller', 'width' => '87' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/serpis.jpg', array('alt'=>'serpis', 'width' => '94' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/schar.png', array('alt'=>'schar', 'width' => '72' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/smirnoff.jpg', array('alt'=>'smirnoff', 'width' => '83' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/sounza.png', array('alt'=>'sounza', 'width' => '93' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/unilever.png', array('alt'=>'unilever', 'width' => '66' , 'height' => '60'));?>
	        	</li>
	        	<li>
	        		<? echo  $this->Html->image('/theme/Bonzzay/images/logos/walmart.jpg', array('alt'=>'walmart', 'width' => '229' , 'height' => '60'));?>
	        	</li>
	        </ul>
           

        </div>
        <div class="clear"></div>
    </div>
</section>
<!-- container ends -->