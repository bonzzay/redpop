<!-- container starts -->
<section class="plv_main">
	<div class="container">
		<h2><? echo __("Contacto");?></h2>
       
        <div id="contact-area">
        	<? if(isset($messagealert)):?>
       		 <div class="messagealert">
       		 	<? echo $messagealert;?>
       		 </div>
       		<? endif;?>
        	<form action="/contacto" role="form" method="post" accept-charset="utf-8">    	
				<label for="Name">Name:</label>
				<input type="text" name="data[Contact][name]" id="Name">
				
	
				<label for="Email">Email:</label>
				<input type="text" name="data[Contact][email]" id="Email">
				
				<label for="Message">Message:</label><br>
				<textarea name="data[Contact][message]" rows="20" cols="20" id="Message"></textarea>

				<input type="submit" name="submit" value="Submit" class="submit-button">
        	</form>
        </div>
		<div class="clear"></div>
        <!--div>
            <p>Redpop, S.L.</p>
			<p>Calle Hermanos Granda 1,</p>
			<p>28022 Madrid, España</p>
				
			<p>info@redpop.es</p>	
			<p>+34 91 755 80 19</p>
			<div><p>Para tener nuestra licencia, entre en contacto con nosotros: joanatavares@displayflash.com (Europa y Asia), lizbeth@displayflash.com (Norte y Sudamerica)</p></div>

        </div-->
        <div class="clear"></div>
    </div>
</section>
<!-- container ends -->

