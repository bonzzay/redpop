<!-- container starts -->
<section class="plv_main">
	<div class="container">
		<h2><? echo __("NEWS");?></h2>
        
            <? $actual= 0;?>
            <? foreach($posts as $post):?>
                     <div class="new">
                        <h3><a href="<? echo $this->Html->url('/');?>news/<? echo $post['Post']['slug'];?>"><? echo $post['Post']['title'];?></a></h3>
                        <p>
                           <? echo $post['Post']['small_text'];?> 
                        </p>

                        <hr>
                         
                     </div>          
                    <div class="clear"></div>           
                
             
            <? endforeach;?>
            
        	
       
        
        <ul class="pagination">
        <?php
            echo $this->Paginator->prev('< ' . __('Previous'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
            echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
            echo $this->Paginator->next(__('Next') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
        ?>
                </ul><!-- /.pagination -->
    </div>
</section>
<!-- container ends -->