
<div id="content" class="row">
<ul class="breadcrumb">
	<li><a href="<?php echo $this->Html->url('/'); ?>" class="glyphicons home"><i></i> Home</a></li>
	<li class="divider"></li>
	<li><?php echo __('Categorias'); ?></li>
</ul>
<div class="separator bottom"></div>
<!-- // Breadcrumb END -->

<h3><?php echo __('Categorias'); ?></h3>
	<div class="innerLR">
		<div class="widget">
		
			<div class="widget-head">
				<h4 class="heading"><?php echo __('Categorias'); ?></h4>
			</div>
			<div class="widget-timeline">

				<ul id="my-list-artist" class="list-timeline">
				<?php foreach ($categorylist as $key=>$value): ?>
					<?
					$editurl = $this->Html->link("Editar", array('action'=>'edit', $key));
				    $upurl = $this->Html->link("▲", array('action'=>'moveup', $key));
				    $downurl = $this->Html->link("▼", array('action'=>'movedown', $key));
				    $deleteurl = $this->Html->link("Eliminar", array('action'=>'delete', $key));
				    echo "<li>[$editurl|$upurl|$downurl|$deleteurl] $value</li>";
				    ?>
				<?php endforeach; ?>
				</ul>



				
			</div><!-- /.table-responsive -->
			
		</div><!-- /.index -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#content .row-fluid -->