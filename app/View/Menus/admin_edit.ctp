<div id="content" class="row">
	<ul class="breadcrumb">
		<li><a href="<?php echo $this->Html->url('/'); ?>" class="glyphicons home"><i></i> Home</a></li>
		<li class="divider"></li>
		<li><?php echo __('Admin Edit Menu'); ?></li>
	</ul>
	<div class="separator bottom"></div>
<!-- // Breadcrumb END -->

	<h3><?php echo __('Admin Edit Menu'); ?></h3>

	
		<div class="innerLR">

		<?php echo $this->Form->create('Menu', array('role' => 'form', 'class' => 'form-horizontal')); ?>
	
		<!--form class="form-horizontal" style="margin-bottom: 0;" id="validateSubmitForm" method="get" autocomplete="off" novalidate="novalidate"-->
		
		<!-- Widget -->
			<div class="widget">
			
				<!-- Widget heading -->
				<div class="widget-head">
					<h4 class="heading"><?php echo __('Admin Edit Menu'); ?></h4>
				</div>
								<!-- // Widget heading END -->
				
								<div class="widget-body row">
									<div class="col-md-12">
															
										<div class="form-group">
						<?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => 'Nombre')); ?>
					</div><!-- .form-group -->
					
					
					
					<div class="form-group">
						<label>Link</label>		
						<div class="input-group">
						<? 
							$valor = '';
							if($menu['Menu']['model'] != null){
								$valor = '#'.$menu['Menu']['model'].'#'.$menu['Menu']['foreign_key'];
							}else{
								$valor = $menu['Menu']['url'];
							}


						?>
						    <input class="form-control col-md-6" id="appendedInputButtons" type="text" placeholder="Placeholder .." name="data[Menu][link]" value="<? echo $valor ;?>">
						    <div class="input-group-btn">
							  	<button class="btn btn-default" type="button" data-toggle="modal" href="#linkModal"><i class="icon-link"></i></button>
							  	
							</div>

						</div>
					</div>

					<div class="form-group">
						<?php echo $this->Form->input('position', array('class' => 'form-control', 'type'=>'hidden', 'value' => $menu['Menu']['position']  )); ?>
					</div><!-- .form-group -->

									</div>
										
									<!-- // Row END -->
									
										
								</div>
								<!-- // Row END -->
				
					<hr class="separator">
					
					<!-- Form actions -->
					<div class="form-actions">

						<button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Guardar</button>
						
					</div>
					<!-- // Form actions END -->
					
			</div>
		</div>
		<!-- // Widget END -->
		
				<?php echo $this->Form->end(); ?>
			
	</div><!-- /#page-content .col-sm-9 -->
<!-- modal windows MAP-->
        <div class="modal fade" id="linkModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title"><? echo __('Seleccion el contenido');?></h4>
                    </div>
                    <div class="modal-body">
                      <h4> Contenidos </h4> 
                       <? foreach ($contents as $content):?>
                       <a href="#Content#<? echo $content['Content']['id'];?>" class="addlinka"><? echo $content['Content']['title'];?></a>, 
                       	
                       <?endforeach;?>

                       <h4> Páginas estáticas </h4> 
                       <? foreach ($statics as $clave=>$valor):?>
                       <a href="<? echo $clave;?>" class="addlinka"><? echo $valor;?></a>, 
                       	
                       <?endforeach;?>
                       	
                     
                    </div>
                    
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->