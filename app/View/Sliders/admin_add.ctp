<div id="content" class="row">
	<ul class="breadcrumb">
		<li><a href="<?php echo $this->Html->url('/'); ?>" class="glyphicons home"><i></i> Home</a></li>
		<li class="divider"></li>
		<li><?php echo __('Nuevo Slider'); ?></li>
	</ul>
	<div class="separator bottom"></div>
<!-- // Breadcrumb END -->

	<h3><?php echo __('Nuevo Slider'); ?></h3>

	
		<div class="innerLR">

		<?php echo $this->Form->create('Slider', array('role' => 'form', 'class' => 'form-horizontal','enctype'=>'multipart/form-data')); ?>
	
		<!--form class="form-horizontal" style="margin-bottom: 0;" id="validateSubmitForm" method="get" autocomplete="off" novalidate="novalidate"-->
		
		<!-- Widget -->
			<div class="widget">
			
				<!-- Widget heading -->
				<div class="widget-head">
					<h4 class="heading"><?php echo __('Nuevo Slider'); ?></h4>
				</div>
								<!-- // Widget heading END -->
				
								<div class="widget-body row">
									<div class="col-md-12">
					<div class="widget-body left">
						<h4>Activo</h4>
						<div class="make-switch" data-on="default" data-off="default">
						
						<input type="checkbox" name="data[Slider][active]" value="1">
						</div>
					</div><!-- .form-group -->

					
					<div class="form-group">
						<?php echo $this->Form->input('title', array('class' => 'form-control', 'label' => 'Título')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('subtitle', array('class' => 'form-control', 'label' => 'Subtítulo')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('image', array('class' => 'form-control','type' => 'file', 'label' => 'Imágen')); ?>
					</div><!-- .form-group -->
					
					<div class="form-group">
						<?php echo $this->Form->input('link', array('class' => 'form-control', 'label' => 'Link')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('description', array('class' => 'form-control', 'label' => 'Descripción')); ?>
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('orden', array('class' => 'form-control', 'label' => 'Orden')); ?>
					</div><!-- .form-group -->
					<!-- .form-group -->

									</div>
										
									<!-- // Row END -->
									
										
								</div>
								<!-- // Row END -->
				
					<hr class="separator">
					
					<!-- Form actions -->
					<div class="form-actions">

						<button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Guardar</button>
						
					</div>
					<!-- // Form actions END -->
					
			</div>
		</div>
		<!-- // Widget END -->
		
				<?php echo $this->Form->end(); ?>
			
	</div><!-- /#page-content .col-sm-9 -->