<div id="content" class="row">
<ul class="breadcrumb">
	<li><a href="<?php echo $this->Html->url('/'); ?>" class="glyphicons home"><i></i> Home</a></li>
	<li class="divider"></li>
	<li><?php echo __('Slider'); ?></li>
</ul>
<div class="separator bottom"></div>
<!-- // Breadcrumb END -->

<h3><?php  echo __('Slider'); ?></h3>
	<div class="innerLR">
		<div class="widget">
		
			<div class="widget-head">
				<h4 class="heading"><?php  echo __('Slider'); ?></h4>
			</div>
			<div class="widget-body">
				<table cellpadding="0" cellspacing="0" class="dynamicTable colVis table table-striped table-bordered table-condensed table-white dataTable">
					<tbody>
						<tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($slider['Slider']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Activo'); ?></strong></td>
		<td>
			<?php echo h($slider['Slider']['active']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Título'); ?></strong></td>
		<td>
			<?php echo h($slider['Slider']['title']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Subtítulo'); ?></strong></td>
		<td>
			<?php echo h($slider['Slider']['subtitle']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Imágen'); ?></strong></td>
		<td>
			<?php echo h($slider['Slider']['image']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Link'); ?></strong></td>
		<td>
			<?php echo h($slider['Slider']['link']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Creado'); ?></strong></td>
		<td>
			<?php echo h($slider['Slider']['created']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Modificado'); ?></strong></td>
		<td>
			<?php echo h($slider['Slider']['modified']); ?>
			&nbsp;
		</td>
</tr>					</tbody>
				</table>
			</div><!-- /.table-responsive -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#content .row-fluid -->
