<div id="content" class="row">
<ul class="breadcrumb">
	<li><a href="<?php echo $this->Html->url('/'); ?>" class="glyphicons home"><i></i> Home</a></li>
	<li class="divider"></li>
	<li><?php echo __('Contents'); ?></li>
</ul>
<div class="separator bottom"></div>
<!-- // Breadcrumb END -->

<h3><?php  echo __('Content'); ?></h3>
	<div class="innerLR">
		<div class="widget">
		
			<div class="widget-head">
				<h4 class="heading"><?php  echo __('Content'); ?></h4>
			</div>
			<div class="widget-body">
				<table cellpadding="0" cellspacing="0" class="dynamicTable colVis table table-striped table-bordered table-condensed table-white dataTable">
					<tbody>
						<tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($content['Content']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Title'); ?></strong></td>
		<td>
			<?php echo h($content['Content']['title']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Slug'); ?></strong></td>
		<td>
			<?php echo h($content['Content']['slug']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Isactive'); ?></strong></td>
		<td>
			<?php echo h($content['Content']['isactive']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Seo Title'); ?></strong></td>
		<td>
			<?php echo h($content['Content']['seo_title']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Seo Description'); ?></strong></td>
		<td>
			<?php echo h($content['Content']['seo_description']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Seo Keys'); ?></strong></td>
		<td>
			<?php echo h($content['Content']['seo_keys']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Text'); ?></strong></td>
		<td>
			<?php echo h($content['Content']['text']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Image'); ?></strong></td>
		<td>
			<?php echo h($content['Content']['image']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Image Dir'); ?></strong></td>
		<td>
			<?php echo h($content['Content']['image_dir']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($content['Content']['created']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Modified'); ?></strong></td>
		<td>
			<?php echo h($content['Content']['modified']); ?>
			&nbsp;
		</td>
</tr>					</tbody>
				</table>
			</div><!-- /.table-responsive -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#content .row-fluid -->
