<div id="content" class="row">
	<ul class="breadcrumb">
		<li><a href="<?php echo $this->Html->url('/'); ?>" class="glyphicons home"><i></i> Home</a></li>
		<li class="divider"></li>
		<li><?php echo __('Editar Faq'); ?></li>
	</ul>
	<div class="separator bottom"></div>
<!-- // Breadcrumb END -->

	<h3><?php echo __('Editar Faq'); ?></h3>

	
		<div class="innerLR">

		<?php echo $this->Form->create('Faq', array('role' => 'form', 'class' => 'form-horizontal')); ?>
	
		<!--form class="form-horizontal" style="margin-bottom: 0;" id="validateSubmitForm" method="get" autocomplete="off" novalidate="novalidate"-->
		
		<!-- Widget -->
			<div class="widget">
			
				<!-- Widget heading -->
				<div class="widget-head">
					<h4 class="heading"><?php echo __('Editar Faq'); ?></h4>
				</div>
								<!-- // Widget heading END -->
				
								<div class="widget-body row">
									<div class="col-md-12">
					<div class="form-group">
						<?php echo $this->Form->input('id', array('class' => 'form-control')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('faqcategory_id', array('class' => 'selectpicker col-md-12', 'label' => 'Categoría')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('questions', array('class' => 'form-control', 'label' => 'Pregunta')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<? 
							$this->Wysiwyg->changeEditor('Ck');
						?>
						<label>Respuesta</label>
						<? echo $this->Wysiwyg->textarea('Faq.answer');?>
					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('orders', array('class' => 'form-control', 'label' => 'Orden')); ?>
					</div><!-- .form-group -->

									</div>
										
									<!-- // Row END -->
									
										
								</div>
								<!-- // Row END -->
				
					<hr class="separator">
					
					<!-- Form actions -->
					<div class="form-actions">

						<button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Guardar</button>
						
					</div>
					<!-- // Form actions END -->
					
			</div>
		</div>
		<!-- // Widget END -->
		
				<?php echo $this->Form->end(); ?>
			
	</div><!-- /#page-content .col-sm-9 -->