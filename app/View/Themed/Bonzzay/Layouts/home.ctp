<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

$cakeDescription = __d('cake_dev', 'Redpop');
?>
<?php echo $this->Html->docType('html5'); ?> 
<html>
	<head>
		<?php echo $this->Html->charset(); ?>
		<title>
			<?php echo $cakeDescription ?>:
			<?php echo $title_for_layout; ?>
		</title>
		<?php
			echo $this->Html->meta('icon');
			
			echo $this->fetch('meta');

			echo $this->Html->css('reset');
			echo $this->Html->css('style');
			echo $this->Html->css('responsive');
			echo $this->Html->css('jquery.bxslider');
			
			
		?>
		
		


				

	
	</head>

	<body>

		<div class="wrapper">
			<?php echo $this->element('menu/head'); ?>
			<?php echo $this->element('menu/menu'); ?>
			<?php echo $this->element('menu/homebanner'); ?>
			<div class="main_container">
    			<?php echo $this->fetch('content'); ?>
    		</div>
    		<?php echo $this->element('menu/footer'); ?>
    			
		</div>
		
		
	<?php
		echo $this->Html->script('libs/jquery-1.11.2.min');
		echo $this->Html->script('libs/jquery.bxslider.min');
	?>


	<script type="text/javascript">
		$(document).ready(function(){
		  $('.bxslider').bxSlider({
		 auto:true,
			
			});
		});
	</script>

	<script type="text/javascript">
		$(document).ready(function(){
			$("nav ul li ul").parent().prepend('<span></span>');
			$("nav ul li span").click(function() {
				$(this).toggleClass("active");
				$(this).parent().find('ul').slideToggle();
			$(this).parent().find('ul ul').hide();
			$(this).parent().siblings().find('span').removeClass("active");
			});

			$('.menu_icon').click(function() {$('nav').slideToggle();});
		});
	</script>
	</body>
</html>