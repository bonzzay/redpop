<div class="banner">
    	<div class="slider">
        	<ul class="bxslider">
            <? foreach($sliders as $slider):?>
                <li>

                    <img src="<? echo $this->Html->url('/');?>files/slider/image/<? echo $slider['Slider']['image_dir'];?>/thumb_<? echo $slider['Slider']['image'];?>" width="1280" height="500" alt="<? echo strtoupper ($slider['Slider']['title']);?>" class="imgpost">
                
                    <div class="banner_in">
                        <div class="container">
                            <h3><? echo $slider['Slider']['title'];?> <br><? echo $slider['Slider']['subtitle'];?></h3>
                        </div>
                    </div>
                </li>

            <? endforeach;?>
            	
            </ul>
        </div>
    </div>