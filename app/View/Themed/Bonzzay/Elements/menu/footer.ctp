<!-- footer starts -->
<footer>
	<div class="container">
    	<div class="footer_logo"><a href="#">
        <? echo  $this->Html->image('/theme/Bonzzay/images/redpop.png', array('alt'=>'power by redpop', 'width' => '136' , 'height' => '48'));?></a></div>
        <div class="footer_nav1">
        	<h3><? echo __("SOLUTIONS");?></h3>
        	<ul>
                <? foreach($footerproduct as $footer):?>
                    <li><a href="<? echo $this->Html->url('/');?><? echo $footer['Category']['slug'];?>"><? echo $footer['Category']['name'];?></a></li>
                <? endforeach;?>
            	
            </ul>
            <div class="clear"></div>
        </div>
        <div class="footer_nav1">
            <h3><? echo __("ABOUT US");?></h3>
        	<ul>
            	<? foreach($footerabout as $footer):?>
                    <li><a href="<? echo $this->Html->url('/');?><? echo $footer['Page']['slug'];?>"><? echo $footer['Menu']['name'];?></a></li>
                <? endforeach;?>
            </ul>
            <div class="clear"></div>
        </div>
        <div class="footer_nav1">
            <h3><? echo __("SUPPORT");?></h3>
        	<ul>
            	
                
                
                <li><a href="mailto:info@qracks.com "><? echo __("info@qracks.com ");?></a></li>
            </ul>
            <div class="clear"></div>
        </div>
        <div class="footer_nav2">
            <h3><? echo __("NEWSLETTER");?></h3>
            <p><? echo __("Receive all the news and promotions. We will not provide your data to third parties.");?> <span><? echo __("Accept Privacy Policy");?></span> </p>
            
            <form action="/newsletter" role="form" method="post" accept-charset="utf-8">  
            <input type="text" class="input_mail" name="data[Mailchimp][email]" value="name@example.com" onclick="if(this.value=='name@example.com'){this.value=''}" onblur="if(this.value==''){this.value='name@example.com'}">
            <input type="submit" class="sus" value="<? echo __("TO SUBSCRIBE"); ?>">
            </form>
        </div>
        <div class="clear"></div>
    </div>
</footer>
<div class="copy">
   <!-- container starts -->
    <div class="container">
        <div class="copy_in">
            <p>&copy;  2016 Qracks. <? echo __("ALL RIGHTS RESERVED.");?> | <a href="<? echo $this->Html->url('/');?>copyright"><? echo __("Copyright");?></a></p>
            
            <div class="clear"></div>
        </div>
    </div>
    <!-- container ends -->
</div>