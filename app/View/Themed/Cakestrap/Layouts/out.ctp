<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

$cakeDescription = __d('cake_dev', 'Redpop Admin');
?>
<?php echo $this->Html->docType('html5'); ?> 
<html>
	<head>
		<?php echo $this->Html->charset(); ?>
		<title>
			<?php echo $cakeDescription ?>:
			<?php echo $title_for_layout; ?>
		</title>
		<?php
			echo $this->Html->meta('icon');
			
			echo $this->fetch('meta');

			echo $this->Html->css('bootstrap');
			echo $this->Html->css('bootstrap-select');
			//echo $this->Html->css('main');
			echo $this->Html->css('glyphicons_social');
			echo $this->Html->css('glyphicons_filetypes');
			echo $this->Html->css('glyphicons_regular');
			echo $this->Html->css('font-awesome.min');
			echo $this->Html->css('uniform.default');
			echo $this->Html->css('prettyPhoto');
			echo $this->Html->css('jquery.easy-pie-chart');
			
			echo $this->Html->css('select2');



			


			echo $this->Html->css('style-light');
			

			echo $this->fetch('css');
			
			
			echo $this->Html->script('libs/less.min');

			
			echo $this->fetch('script');
		?>
		<script>
var primaryColor = '#e25f39',
	dangerColor = '#bd362f',
	successColor = '#609450',
	warningColor = '#ab7a4b',
	inverseColor = '#45484d';
</script>

<!-- Themer -->
<script>
var themerPrimaryColor = primaryColor;
</script>


	
	
	</head>

	<body class="login">

		<?php echo $this->Session->flash(); ?>
		<?php echo $this->fetch('content'); ?>

		
	</body>


	<?php
		echo $this->Html->script('libs/jquery-1.10.2.min');
		echo $this->Html->script('libs/jquery.ui.touch-punch.min');
		echo $this->Html->script('libs/modernizr');
		echo $this->Html->script('libs/bootstrap.min');
		echo $this->Html->script('libs/bootstrap-select');
		echo $this->Html->script('libs/jquery.slimscroll.min');
		echo $this->Html->script('libs/common');
		echo $this->Html->script('libs/holder');
		echo $this->Html->script('libs/jquery.uniform.min');
		echo $this->Html->script('libs/jquery.prettyPhoto');
		echo $this->Html->script('libs/select2');
		echo $this->Html->script('libs/jquery.easypiechart');
		echo $this->Html->script('libs/jquery.sparkline.min');
		echo $this->Html->script('libs/jquery.flot');
		echo $this->Html->script('libs/jquery.flot.pie');

	?>

	
	
	

</html>