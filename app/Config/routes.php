<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
 
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'pages', 'action' => 'home'));
	Router::connect('/about', array('controller' => 'pages', 'action' => 'about'));
	Router::connect('/solutions', array('controller' => 'pages', 'action' => 'solutions'));
	Router::connect('/clients', array('controller' => 'pages', 'action' => 'clients'));
	Router::connect('/copyright', array('controller' => 'pages', 'action' => 'copyright'));
	Router::connect('/newsletter', array('controller' => 'pages', 'action' => 'newsletter'));

	


	

	Router::connect('/faq', array('controller' => 'pages', 'action' => 'faq'));
	Router::connect('/faq/*', array('controller' => 'pages', 'action' => 'faq'));

	Router::connect('/news', array('controller' => 'pages', 'action' => 'posts'));
	Router::connect('/news/*', array('controller' => 'pages', 'action' => 'posts'));
	Router::connect('/contacto', array('controller' => 'pages', 'action' => 'contact'));



/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

	Router::connect('/admin', array('controller' => 'users', 'action' => 'login', 'admin' => true));
	
	Router::connect('/admin/menus/add', array('controller' => 'menus', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/menus/delete', array('controller' => 'menus', 'action' => 'delete', 'admin' => true));
	Router::connect('/admin/menus/edit', array('controller' => 'menus', 'action' => 'edit', 'admin' => true));
	Router::connect(
	    '/admin/menus/:id', // E.g. /blog/3-CakePHP_Rocks
	    array('controller' => 'menus', 'action' => 'index', 'admin' => true),
	    array(
		        // order matters since this will simply map ":id" to
		        // $articleId in your action
		        //'pass' => array('slugc', 'slug'),
	    		'pass' => array('id'),
		        'id' => '[0-9]+'
		    )
	);

	Router::connect('/admin/categories', array('controller' => 'categories', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/categories/add', array('controller' => 'categories', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/categories/delete', array('controller' => 'categories', 'action' => 'delete', 'admin' => true));
	Router::connect('/admin/categories/edit', array('controller' => 'categories', 'action' => 'edit', 'admin' => true));
	Router::connect('/admin/contents', array('controller' => 'contents', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/contents/add', array('controller' => 'contents', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/contents/delete', array('controller' => 'contents', 'action' => 'delete', 'admin' => true));
	Router::connect('/admin/contents/edit', array('controller' => 'contents', 'action' => 'edit', 'admin' => true));
	Router::connect('/admin/contents/view', array('controller' => 'contents', 'action' => 'view', 'admin' => true));
	Router::connect('/admin/products', array('controller' => 'products', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/products/add', array('controller' => 'products', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/products/delete', array('controller' => 'products', 'action' => 'delete', 'admin' => true));
	Router::connect('/admin/products/edit', array('controller' => 'products', 'action' => 'edit', 'admin' => true));
	Router::connect('/admin/users', array('controller' => 'users', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/users/add', array('controller' => 'users', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/users/delete', array('controller' => 'users', 'action' => 'delete', 'admin' => true));
	Router::connect('/admin/users/edit', array('controller' => 'users', 'action' => 'edit', 'admin' => true));
	Router::connect('/admin/posts', array('controller' => 'posts', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/posts/add', array('controller' => 'posts', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/posts/delete', array('controller' => 'posts', 'action' => 'delete', 'admin' => true));
	Router::connect('/admin/posts/edit', array('controller' => 'posts', 'action' => 'edit', 'admin' => true));
	Router::connect('/admin/faqs', array('controller' => 'faqs', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/faqs/add', array('controller' => 'faqs', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/faqs/delete', array('controller' => 'faqs', 'action' => 'delete', 'admin' => true));
	Router::connect('/admin/faqs/edit', array('controller' => 'faqs', 'action' => 'edit', 'admin' => true));
	Router::connect('/admin/faqcategories', array('controller' => 'faqcategories', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/faqcategories/add', array('controller' => 'faqcategories', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/faqcategories/delete', array('controller' => 'faqcategories', 'action' => 'delete', 'admin' => true));
	Router::connect('/admin/faqcategories/edit', array('controller' => 'faqcategories', 'action' => 'edit', 'admin' => true));

	Router::connect('/admin/sliders', array('controller' => 'sliders', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/sliders/add', array('controller' => 'sliders', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/sliders/delete', array('controller' => 'sliders', 'action' => 'delete', 'admin' => true));
	Router::connect('/admin/sliders/edit', array('controller' => 'sliders', 'action' => 'edit', 'admin' => true));
	


	Router::connect(
	    '/:slug', // E.g. /blog/3-CakePHP_Rocks
	    array('controller' => 'pages', 'action' => 'category'),
	    array(
		        // order matters since this will simply map ":id" to
		        // $articleId in your action
		        'pass' => array('slug'),
		        //'id' => '[0-9]+'
		    )
	);
	Router::connect(
	    '/:slug/:slugp', // E.g. /blog/3-CakePHP_Rocks
	    array('controller' => 'pages', 'action' => 'product'),
	    array(
		        // order matters since this will simply map ":id" to
		        // $articleId in your action
		        'pass' => array('slug', 'slugp'),
		        //'id' => '[0-9]+'
		    )
	);
/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
